import 'dart:convert';

void main() {
  // Creating Instances of winning objects
  var apps = [
    AppN("FNB\t", 2012),
    AppN("SnapScan", 2013),
    AppN("LIVE Inspect", 2014),
    AppN("WumDrop\t", 2015),
    AppN("Domestly", 2016),
    AppN("Shyft\t", 2017),
    AppN("Khula ecosystem", 2018),
    AppN("Naked Insurance", 2019),
    AppN("EasyEquities", 2020),
    AppN("Edtech\t", 2021)
  ];
//Printing all the Winning Apps Objects
  for (int i = 0; i < 10; i++) {
    print(apps[i].appName + "\t ${apps[i].appYear}");
  }

//Printing the 2017 and 2018 winners
  for (int i = 0; i < 10; i++) {
    if (apps[i].appYear == 2017 || apps[i].appYear == 2018) {
      print(apps[i].appName);
    }
  }
  //Print Array Length
  print(apps.length);
}
//App class
class AppN {
  var appName;
  var appYear;
  void setAppName(String name) {
    name = this.appName;
  }

  void setAppYear(int appyear) {
    appyear = appYear;
  }

  AppN(String name, int year) {
    this.appName = name;
    this.appYear = year;
  }
}

