import 'dart:convert';

void main() {
  // Creating Instances of App
  var app1 = new App("EasyEquities", "Banking", "Charles Savage", "2020");
  app1.PrintAppDetails();
  print(app1.capital());
}

class App {
  var appName;
  var AppCategory;
  var devoloperName;
  var appYear;

  void PrintAppDetails() {
    print("Name :" + this.appName);
    print("Category :" + this.AppCategory);
    print("Developer :" + this.devoloperName);
    print("Year :" + this.appYear);
  }

  String capital() {
    String n = this.appName;
    return n.toUpperCase();
  }

  App(String name, String appCat, String dev, String year) {
    this.appName = name;
    this.AppCategory = appCat;
    this.devoloperName = dev;
    this.appYear = year;
  }
}

